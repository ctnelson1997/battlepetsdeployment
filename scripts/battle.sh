#!/bin/bash
cd "/var/lib/jenkins/workspace/BattlePets_Dev/manager/src/main/java/"
javac $(find . -name "*.java")
cp -rf "/battlepets/dev/includes/." .
docker build -t battlepets/dev .
docker run -i battlepets/dev '0' <<< $(cat "/var/lib/jenkins/workspace/BattlePets_Dev/manager/src/main/java/input.in")
