cd /var/lib/jenkins/jobs/BattlePets_Dev
DEV_BUILD_NUM=$(head -n 1 nextBuildNumber)
cd builds/$(expr $DEV_BUILD_NUM - 1)
cp log log.bak
rm log
tac log.bak | grep -F -m1 -B 999999 'Win Counts' | head -n -1 | tac > log
